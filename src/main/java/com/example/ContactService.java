package com.example;



public class ContactService {
    private IContactDao contactDao = new ContactDao();

    public void creerContact(String nom){
        if(nom == null || nom.trim().length() < 3 || nom.trim().length() > 10) 
            throw new IllegalArgumentException("Le nom doit être supérieur ou égale à 3 charactères"); 

        nom = nom.toLowerCase();
        if(contactDao.isContactExist(nom)){
            throw new ContactDuplicationException();
        }
        contactDao.addContact(nom);
    }

}
