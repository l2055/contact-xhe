package com.example;

public class ContactDuplicationException extends RuntimeException {

    public ContactDuplicationException() {
        super("Le contact existe déjà");
    }
}
