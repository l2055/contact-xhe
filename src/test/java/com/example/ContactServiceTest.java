package com.example;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Assertions;


public class ContactServiceTest {
    private ContactService contactService = new ContactService();

    @Test
    void shouldFailTooShort() {
        assertThrows(IllegalArgumentException.class, 
        () -> contactService.creerContact("ab"));
    }

    @Test
    void shouldFailTooLong() {
        Assertions.assertThrows(IllegalArgumentException.class, 
        () -> contactService.creerContact("abcdefishep"));
    }


    @Test
    void shouldFailEmpty() {
        Assertions.assertThrows(IllegalArgumentException.class, 
        () -> contactService.creerContact(""));
    }


    @Test
    void shouldFailBlank() {
        Assertions.assertThrows(IllegalArgumentException.class, 
        () -> contactService.creerContact("   "));
    }

    @Test
    void shouldFailNull() {
        Assertions.assertThrows(IllegalArgumentException.class, 
        () -> contactService.creerContact(null));
    }

    @Test
    void shouldFailDuplicate() {
        contactService.creerContact("thierry");
        Assertions.assertThrows(ContactDuplicationException.class, 
        () -> contactService.creerContact("thierry"));
    }


}
